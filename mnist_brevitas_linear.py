#!/usr/bin/env python3

import torch
import torch.nn.functional as F
import torchvision
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt
import time
import random


import torch.nn as nn
from brevitas.quant_tensor import pack_quant_tensor
import brevitas.nn as qnn
from brevitas.core.quant import QuantType
from brevitas.core.restrict_val import RestrictValueType
from brevitas.core.scaling import ScalingImplType
from brevitas.core.stats import StatsOp

class MyQuantReLU(nn.Module):
    def __init__(self, max_val, quant_type, bit_width, scaling_impl_type=ScalingImplType.CONST):
        super(MyQuantReLU, self).__init__()
        self._min_val_act = -1
        self._max_val_act = 1 - 2/(2**bit_width)
        self._min_val = 0
        self._max_val = max_val
        self._act = qnn.QuantHardTanh(scaling_impl_type=ScalingImplType.CONST, min_val=self._min_val_act, max_val=self._max_val_act, quant_type=quant_type, bit_width=bit_width)
        self._scale = (self._max_val - self._min_val) / (self._max_val_act - self._min_val_act)

    def forward(self, x):
        x = self._act(x / self._scale - 1)
        x = self._scale * x
        x = x + self._scale
        return x


WEIGHT_QUANT_TYPE = QuantType.BINARY
WEIGHT_BIT_WIDTH  = 1

ACTIVATION_TYPE       = MyQuantReLU
ACTIVATION_QUANT_TYPE = QuantType.INT
ACTIVATION_BIT_WIDTH  = 2
ACTIVATION_MAX_VAL    = 6
ACTIVATION_MIN_VAL    = 0
ACTIVATION_SCALING_IMPL_TYPE = ScalingImplType.CONST

class MnistNetLin(nn.Module):
    def __init__(self):
        super(MnistNetLin, self).__init__()
        self.linear1 = qnn.QuantLinear(28*28, 32, bias=True, weight_quant_type=WEIGHT_QUANT_TYPE, weight_bit_width=WEIGHT_BIT_WIDTH)
        self.linear2 = qnn.QuantLinear(32, 32,    bias=True, weight_quant_type=WEIGHT_QUANT_TYPE, weight_bit_width=WEIGHT_BIT_WIDTH)
        self.linear3 = qnn.QuantLinear(32, 10,    bias=True, weight_quant_type=WEIGHT_QUANT_TYPE, weight_bit_width=WEIGHT_BIT_WIDTH)
        self.activation_1 = ACTIVATION_TYPE(max_val=ACTIVATION_MAX_VAL, quant_type=ACTIVATION_QUANT_TYPE, bit_width=ACTIVATION_BIT_WIDTH, scaling_impl_type=ACTIVATION_SCALING_IMPL_TYPE)
        self.activation_2 = ACTIVATION_TYPE(max_val=ACTIVATION_MAX_VAL, quant_type=ACTIVATION_QUANT_TYPE, bit_width=ACTIVATION_BIT_WIDTH, scaling_impl_type=ACTIVATION_SCALING_IMPL_TYPE)
        self.bn1 = nn.BatchNorm1d(32)
        self.bn2 = nn.BatchNorm1d(32)
        #self.dropout = nn.Dropout()

    def forward(self, x):
        x = x.view(x.shape[0], -1)
        x = self.linear1(x)
        x = self.bn1(x)
        x = self.activation_1(x)
        x = self.linear2(x)
        x = self.bn2(x)
        x = self.activation_1(x)
        #x = self.dropout(x)
        x = self.linear3(x)
        return x


def train(model, device, train_loader, optimizer, epoch, criterion):
    model.train()
    log_interval = 100
    running_loss = 0.0
    for batch_idx, (inputs, labels) in enumerate(train_loader):
        #inputs = inputs.view(-1, 28*28)
        inputs, labels = inputs.to(device), labels.to(device)

        optimizer.zero_grad()
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        running_loss += loss.item()
        if batch_idx % log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(inputs), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))

def test(model, device, test_loader, criterion):
    model.eval()
    test_loss = 0
    correct = 0

    with torch.no_grad():
        for data, target in test_loader:
            #data = data.view(-1, 28*28)
            data, target = data.to(device), target.to(device)
            output = model(data)
            #test_loss += criterion(output, target, reduction='sum').item()  # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))

def main():
    random_seed = 1
    random.seed(random_seed)
    torch.manual_seed(random_seed)
    torch.cuda.manual_seed_all(random_seed)

    use_cuda = True
    device = torch.device("cuda" if use_cuda else "cpu")

    num_epochs = 2

    net = MnistNetLin()
    net.to(device)

    criterion = nn.CrossEntropyLoss().to(device)
    #optimizer = optim.RMSprop(net.parameters()) #It doesn't work with RMSProp when two last ReLUs are INT-quantized.
    optimizer = optim.Adam(net.parameters())

    # Mean = 0, std = 1 -- it means no normalization.
    # The network does not work with mean = 0.1307, std = 0.3081 which are often used with MNIST
    mnist_mean = 0 
    mnist_std = 1
    transform = torchvision.transforms.Compose([torchvision.transforms.ToTensor(), torchvision.transforms.Normalize((mnist_mean,),(mnist_std,))])
    trainset = torchvision.datasets.MNIST('./data', train=True, download=True, transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=64, shuffle=True)

    testset = torchvision.datasets.MNIST('./data', train=False, download=True, transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=1000, shuffle=True)

    train_time_start = time.time()
    for epoch in range(1, num_epochs + 1):
       train(net, device, trainloader, optimizer, epoch, criterion)
       test(net, device, testloader, criterion)
    print("Training finished! It took {} seconds".format(time.time() - train_time_start))

    checkpoint_loc = "/home/konradl/finn/mnist_checkpoint/mnist_lin.tar"
    torch.save(net.state_dict(), checkpoint_loc)

if __name__ == "__main__":
    main()
